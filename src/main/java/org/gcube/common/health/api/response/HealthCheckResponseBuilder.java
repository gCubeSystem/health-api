package org.gcube.common.health.api.response;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.health.api.Status;
import org.gcube.common.validator.ValidationError;
import org.gcube.common.validator.Validator;
import org.gcube.common.validator.ValidatorFactory;

public class HealthCheckResponseBuilder {

	private HealthCheckResponse healthCheckResponse = new HealthCheckResponse();
		
	protected HealthCheckResponseBuilder(String name) {
		healthCheckResponse.name = name;
	}
	
	public SuccessPart up() {
		healthCheckResponse.status = Status.UP;
		return new SuccessPart();
	}
	
	
	public ErrorPart down() {
		healthCheckResponse.status = Status.DOWN;
		return new ErrorPart();
	}
	
	private void validateResponse() {

		List<String> msgs = new ArrayList<String>();

		Validator validator = ValidatorFactory.validator();

		for (ValidationError error : validator.validate(healthCheckResponse))
			msgs.add(error.toString());

		if (!msgs.isEmpty())
			throw new IllegalStateException("invalid configuration: "+msgs);

	}
	
	public class ErrorPart{
		
		private ErrorPart() {}
		
		public BuildPart error(String message) {
			healthCheckResponse.errorMessage = message;
			return new BuildPart();
		}
		
	}
	
	public class SuccessPart extends BuildPart{
		
		private SuccessPart() {}
		
		public BuildPart info(String info) {
			healthCheckResponse.info = info;
			return new BuildPart();
		}
		
	}

	
	public class BuildPart{
		
		private BuildPart() {};
		
		public HealthCheckResponse build() {
			validateResponse();
			return healthCheckResponse;		
		}
		
	}
	
}
