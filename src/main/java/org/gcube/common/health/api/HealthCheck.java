package org.gcube.common.health.api;

import org.gcube.common.health.api.response.HealthCheckResponse;

public interface HealthCheck {

	String getName();
	
	HealthCheckResponse check();
	
}
