package org.gcube.common.health.api.response;

import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.common.health.api.Status;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;

@JsonInclude(Include.NON_NULL)
public class HealthCheckResponse {
	
	public static HealthCheckResponseBuilder builder(String checkName) {
		return new HealthCheckResponseBuilder(checkName);
	}
	
	protected HealthCheckResponse() {}
	
	//enum ErrorType {UNRECOVERABLE, RECOVERABLE}

	@NotNull @NotEmpty
	protected String name;
	
	@NotNull
	protected Status status;
	
	//protected ErrorType errorType;
	
	protected String errorMessage;
	
	protected String info;

	public Status getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getInfo() {
		return info; 
	}
	
}
