package org.gcube.common.health.api;

public enum Status {

	UP, DOWN, NOT_CALCULATED
}
